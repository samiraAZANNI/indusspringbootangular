package com.projetindus.Entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

public class StaticResult implements java.io.Serializable{

	private Object semaine;
    private Object qtySum;
    private Object division;
    
    
	public StaticResult() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StaticResult(Object semaine, Object qtySum , Object division) {
		super();
		this.semaine = semaine;
		this.qtySum = qtySum;
		this.division = division;
	}
	
	public Object getSemaine() {
		return semaine;
	}

	public void setSemaine(Object semaine) {
		this.semaine = semaine;
	}

	public Object getQtySum() {
		return qtySum;
	}

	public void setQtySum(Object qtySum) {
		this.qtySum = qtySum;
	}

	public Object getDivision() {
		return division;
	}

	public void setDivision(Object division) {
		this.division = division;
	} 
    
}
