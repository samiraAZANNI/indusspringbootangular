package com.projetindus.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "livraison")
public class Livraison{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "qty")
	private float qty;
	@Column(name = "sliv")
	private String sliv;
	@Column(name = "couv")
	private float couv;
	public Livraison() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Livraison(Integer id, float qty, String sliv, float couv) {
		super();
		this.id = id;
		this.qty = qty;
		this.sliv = sliv;
		this.couv = couv;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public float getQty() {
		return qty;
	}
	public void setQty(float qty) {
		this.qty = qty;
	}
	public String getSliv() {
		return sliv;
	}
	public void setSliv(String sliv) {
		this.sliv = sliv;
	}
	public float getCouv() {
		return couv;
	}
	public void setCouv(float couv) {
		this.couv = couv;
	}
		
	
}
