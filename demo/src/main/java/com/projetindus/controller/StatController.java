package com.projetindus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.projetindus.Entity.Livraison;
import com.projetindus.Entity.StaticResult;
import com.projetindus.Service.LivraisonService;


@RestController
@RequestMapping("/statistiques")
public class StatController {
	
	@Autowired
	private LivraisonService livraisonService;

	@GetMapping(value = "/test")
	public String showGreeting() {
		return "Hello world ";
	}

	@GetMapping(value = "/{name}")
	public String showGreeting(@PathVariable("name") String name) {
		return "Hello " + name;
	}
	
	@RequestMapping(value="",method=RequestMethod.GET)
	public List<StaticResult> findLivraisonsByGroupment() {
		return livraisonService.findLivraisonsByGroupment();
	}

}
