package com.projetindus.repisotory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.projetindus.Entity.Livraison;
import com.projetindus.Entity.StaticResult;


@Repository("Livraison")
public interface LivraisRepository extends JpaRepository<Livraison, Integer>{

	@Query(value = "SELECT NEW com.projetindus.Entity.StaticResult(l.sliv , SUM(l.qty) , SUM(l.couv)/COUNT(l.sliv)) FROM Livraison l GROUP by l.sliv")
	public List<StaticResult> findLivraisonsByGroupment();

}
