package com.projetindus.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.projetindus.Entity.Livraison;
import com.projetindus.Entity.StaticResult;
import com.projetindus.repisotory.LivraisRepository;

@Service
public class LivraisonService {
	
	@Autowired
	LivraisRepository livraisonRepository;

	public List<StaticResult> findLivraisonsByGroupment() {
		return livraisonRepository.findLivraisonsByGroupment();
	}

		
	
}
